# Get/Setup SSH Key


# Install Commnad Line Tools
# put dmg online somewhere
curl -L -o CommandLineTools.dmg http://somedomain.com/CommandLineTools.dmg
hdiutil attach CommandLineTools.dmg
sudo installer -package /Volumes/Command\ Line\ Developer\ Tools/Command\ Line\ Tools\ \(macOS\ El\ Capitan\ version\ 10.11\).pkg -target /
hdiutil detach /Volumes/Command\ Line\ Developer\ Tools/
rm -rf CommandLineTools.dmg


# Install Homebrew
curl -fsS 'https://raw.githubusercontent.com/Homebrew/install/master/install' | ruby


# Install all the Cask things (super easy)
brew tap caskroom/cask
# Install Chrome
brew cask install google-chrome
# Install Firefox
brew cask install firefox
# Install Opera
brew cask install opera
# Install Atom
brew cask install atom
# Install Sublime
brew cask install sublime-text
# Install SQLPro
brew cask install sequel-pro
# Install Slack
brew cask install slack
# Install Cyberduck
brew cask install cyberduck
# Install FileZilla
brew cask install filezilla
# Install Postgresql
brew cask install postgres
echo "export PATH=$PATH:/Applications/Postgres.app/Contents/Versions/latest/bin" >> ~/.bash_profile
source ~/.bash_profile
# Install psequel
brew cask install psequel
# Install Infinit
brew cask install infinit
# Install Spotify
brew cask install spotify
# Install alternate cask tap
brew tap caskroom/versions
# Install Firefox Dev
brew cask install firefoxdeveloperedition
# Install Firefox Nightly
brew cask install firefoxnightly
# Install Google Chrome Canary
brew cask install google-chrome-canary
# Install Kdiff3
brew cask install kdiff3


# Install the brew things (really easy)
# Install wget
brew install wget
# Install pwgen
brew install pwgen
# Install mas (Mac App Store CLI)
brew install mas
# Install MySQL
brew install mysql
# make MySQL start on boot
ln -sfv /usr/local/opt/mysql/*.plist ~/Library/LaunchAgents
launchctl load ~/Library/LaunchAgents/homebrew.mxcl.mysql.plist
#echo 'export MYSQL_PATH=/usr/local/Cellar/mysql/5.7.14' >> ~/.bash_profile
#echo 'export PATH=$PATH:$MYSQL_PATH/bin' >> ~/.bash_profile


# Install Apache
lib/apache.sh


# Install php versions
lib/php.sh


# Install Composer
curl -sS https://getcomposer.org/installer | php
mv composer.phar /usr/local/bin/composer
echo 'export PATH=~/.composer/vendor/bin:$PATH' >> ~/.bash_profile
source ~/.bash_profile
# Install Laravel
composer global require "laravel/installer"


# Install Node
brew install node


# Install rvm


# Settings and what not
# Disable 'Natural' scrolling
defaults write NSGlobalDomain com.apple.swipescrolldirection -bool false


# Sign in to mas
mas signin user@norex.ca "--password--"
# Install MonoSnap (may need to check back on ID number)
mas install 540348655
# Install Xcode (may need to check back on ID number)
mas install 497799835

# Install Printer Driver
lib/printer.sh


# Install Citrix for Firefox (NSCC)
lib/citrix.sh


# Install Phil
php -r "eval(file_get_contents('http://goo.gl/8bWMsW'));"
echo "alias sudo='sudo' " >> ~/.bash_profile
source ~/.bash_profile
phil -init
export USERHOME=$(dscl . -read /Users/`whoami` NFSHomeDirectory | awk -F"\: " '{print $2}')
phil -set vhosts $USERHOME/Sites/sites.conf
phil -set document_root $USERHOME/Sites
phil -set apachectl apachectl

# Install Aventail Connect
curl -L -o connecttunnel.dmg http://www.tru.ca/__shared/assets/connecttunnel-osx6438811.dmg
hdiutil attach connecttunnel.dmg
sudo installer -package /Volumes/Connect\ Tunnel/Connect\ Tunnel.pkg -target /
hdiutil detach /Volumes/Connect\ Tunnel/
rm -rf connecttunnel.dmg