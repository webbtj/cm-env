echo "PS1='\\$ '" >> ~/.bash_profile
source ~/.bash_profile

defaults write com.apple.Terminal "Default Window Settings" -string 'Homebrew'
defaults write com.apple.Terminal "Startup Window Settings" -string 'Homebrew'