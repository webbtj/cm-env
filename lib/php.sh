brew install automake autoconf curl pcre bison re2c mhash libtool icu4c gettext jpeg libxml2 mcrypt gmp libevent gd

brew tap homebrew/dupes
brew tap homebrew/versions
brew tap homebrew/homebrew-php
brew install php54
brew unlink php54
brew install php55
brew unlink php55
brew install php56
brew unlink php56
brew install php71 --with-pg-sql --with-apache
brew unlink php71
brew install php70


echo 'export PATH=/usr/local/bin:/usr/local/sbin:$PATH:/Users/your_user/bin' >> ~/.bash_profile
source ~/.bash_profile 

export USERHOME=$(dscl . -read /Users/`whoami` NFSHomeDirectory | awk -F"\: " '{print $2}')

sudo bash -c "cat >> /private/etc/apache2/httpd.conf <<EOF
Include ${USERHOME}/Sites/php.conf
EOF"

mkdir ~/.utilities

cat >> ~/.utilities/phpv.sh <<EOF
currentversion="\`php -r \"error_reporting(0); echo str_replace('.', '', substr(phpversion(), 0, 3));\"\`"
newversion="\$1"
majourversion="\`php -r \"error_reporting(0); echo substr('\$1', 0, 1);\"\`"

brew unlink php\$currentversion
brew link php\$newversion

export USERHOME=\$(dscl . -read /Users/\`whoami\` NFSHomeDirectory | awk -F"\: " '{print \$2}')

echo "LoadModule php\${majourversion}_module \$(brew list php\$newversion | grep libphp)" > \${USERHOME}/Sites/php.conf

sudo apachectl restart
EOF

echo 'alias phpv="~/.utilities/phpv.sh"' >> ~/.bash_profile
source ~/.bash_profile
