# Install dockutil
git clone git@github.com:kcrawford/dockutil.git ~/.utilities/dockutil
chmod +x ~/.utilities/dockutil/scripts/dockutil
echo 'alias dockutil="~/.utilities/dockutil/scripts/dockutil"' >> ~/.bash_profile
source ~/.bash_profile

# Remove stuff we won't use
dockutil --remove "Mail" --no-restart
dockutil --remove "Contacts" --no-restart
dockutil --remove "Calendar" --no-restart
dockutil --remove "Maps" --no-restart
dockutil --remove "Photos" --no-restart
dockutil --remove "Messages" --no-restart
dockutil --remove "FaceTime" --no-restart
dockutil --remove "Photo Booth" --no-restart
dockutil --remove "iTunes" --no-restart
dockutil --remove "iBooks" --no-restart
dockutil --remove "System Preferences" --no-restart

# Add stuff we will use
dockutil --add /Applications/Google\ Chrome.app/ --after 'Launchpad' --no-restart
dockutil --add /Applications/Firefox.app/ --after 'Google Chrome' --no-restart
dockutil --add /Applications/Opera.app/ --after 'Safari' --no-restart
dockutil --add /Applications/Utilities/Terminal.app/ --after 'Opera' --no-restart
dockutil --add /Applications/Utilities/Activity\ Monitor.app/ --after 'Terminal' --no-restart
dockutil --add /Applications/System\ Preferences.app/ --after 'Activity Monitor' --no-restart
dockutil --add /Applications/Cyberduck.app/ --after 'App Store' --no-restart
dockutil --add /Applications/FileZilla.app/ --after 'Cyberduck' --no-restart
dockutil --add /Applications/Sequel\ Pro.app/ --after 'FileZilla' --no-restart
dockutil --add /Applications/PSequel.app/ --after 'Sequel Pro' --no-restart
dockutil --add /Applications/Atom.app/ --after 'PSequel' --no-restart
dockutil --add /Applications/Sublime\ Text.app/ --after 'Atom' --no-restart
dockutil --add /Applications/Mail.app/ --after 'Sublime Text' --no-restart
dockutil --add /Applications/Slack.app/ --after 'Mail' --no-restart
dockutil --add /Applications/Spotify.app/ --after 'Slack'

