#!/bin/sh

## Detect the user who launched the script
usr=$(env | grep SUDO_USER | cut -d= -f 2)
## Exit if the script was not launched by root or through sudo
if [ -z $usr ] && [ $USER = "root" ]
then
    echo "The script needs to run as root" && exit 1
else
    echo "The script shoulds to run as root" && exit 1
fi


# Download driver
sudo -u $usr curl -L -o Mac_MF_Ver1060_00.dmg http://pdisp01.c-wss.com/gdl/WWUFORedirectTarget.do?id=MDEwMDAwNzM0NTAy\&cmp=ACB\&lang=EN
# Mount image
sudo -u $usr hdiutil attach Mac_MF_Ver1060_00.dmg
# Install driver
installer -package /Volumes/Mac_MF_Ver1060_00/MF_Printer_Installer.pkg -target /
# Add printer
sudo -u $usr lpadmin -p Norex_Printer -L "Norex Printer" -E -v lpd://10.50.5.60 -P /Library/Printers/PPDs/Contents/Resources/CNPZBMF4800ZB.ppd.gz
# Unmount image
sudo -u $usr hdiutil detach /Volumes/Mac_MF_Ver1060_00/
# Delete disk image file
sudo -u $usr rm -rf Mac_MF_Ver1060_00.dmg
