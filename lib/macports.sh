curl -L -o MacPorts.pkg https://distfiles.macports.org/MacPorts/MacPorts-2.3.4-10.11-ElCapitan.pkg
sudo installer -package MacPorts.pkg -target /
rm -rf MacPorts.pkg
echo 'export PATH=/opt/local/bin:/opt/local/sbin:$PATH' >> ~/.bash_profile
sudo port install openssl
